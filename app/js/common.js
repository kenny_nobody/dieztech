$(document).ready(function(){

	$('.review__slider-content').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
	});

	$('.popup').magnificPopup({
		type: 'iframe',
	});

	$('.review__link--prev').on('click', function() {
		$('.review__slider-content').slick('slickPrev');
	});
	$('.review__link--next').on('click', function() {
		$('.review__slider-content').slick('slickNext');
	});

	function animateFrontPage(){
		if ($(window).width() >= '1220'){
			new WOW().init();
			$('.header__h1').addClass('animated fadeIn');
			$('.text__content').addClass('fadeIn animated');
			$('.mission__text').addClass('fadeIn animated');
			$('.facts__columns').addClass('fadeIn animated');
		} else {
			return false;
		}
	}

	animateFrontPage();

	var startWindowScroll = 0;

	$("#my-menu").mmenu({
		'navbar': {
			"title": "<span class='logo navline__logo'>#DIEZTech</span>",
		},
		extensions: {
			"all": ["theme-white", "pagedim-black", "border-none", "fx-menu-slide"],
			"(min-height: 600px)": ["listview-large"],
			"(min-height: 900px)": ["listview-huge"]
		}
	}, {
		offCanvas: {
			pageSelector: "#page"
		}
	});

	if($('body').is('.home')){
		var scrollHeightvar = $(window).height();
		var show = true;
		var countbox = ".info__column--content";
		$(window).on("scroll load resize", function(){
			if(!show) return false;                   
			var w_top = $(window).scrollTop();       
			var e_top = $(countbox).offset().top;     
			var w_height = $(window).height();        
			var d_height = $(document).height();     
			var e_height = $(countbox).outerHeight(); 
			if(w_top + scrollHeightvar >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
				$(".spincrement").spincrement({
					thousandSeparator: "",
					duration: 1200,
				});
				show = false;
			}
		});

	};

	var div = $("#page").height();
	var win = $(window).height() + 400;

	if (div > win) {
		
	} else {
		$('.top-scroll').css({'display' : 'none'});
	}



});

function windowReize(){
	var resizeArticle = 1/1;
	$('.works__article').height($('.works__article').width() / resizeArticle);
}

$(window).on('load resize', windowReize);

$(document).on("click", ".autoscroll", function(e){
	$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top }, 1000);
	e.preventDefault();
});